require('dotenv').config();
const express = require('express'); // import
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
//const port = 3000; puerto fijo
const port = process.env.PORT || 3000; // puerto puede ser segun la variable o por 3000
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu9db/collections/';
//const apikey_mlab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;
const usersFile = require('./user.json');


app.listen(port, function(){
  console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//operación get con MLAB (collection)
app.get(URL_BASE + 'users',
  function(req, res) {
    const httpClient = request_json.createClient(URL_mLab);
    console.log("Cliente HTTP mLab creado.");
    let fieldParam = 'f={"_id":0}&';
    httpClient.get('user?' + fieldParam + apikey_mlab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});

app.get(URL_BASE + 'users/:id',
  function (req, res) {
    console.log("GET /colapi/v3/users/:id");
    console.log(req.params.id);
    let id = req.params.id;
    let queryString = 'q={"id_user":' + id + '}&';
    let queryStrField = 'f={"_id":0}&';
    let httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' + queryString + queryStrField + apikey_mlab,
      function(err, respuestaMLab, body){
        console.log("Error: " + err);
        console.log('respuesta MLab: '+ respuestaMLab);
        console.log('Body: '+ body);
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

//operación get user con id con MLAB (collection)
app.get(URL_BASE + 'users/:id/accounts', // user con sus cuentas
  function(req, res) {
    console.log('Get users/:id/accounts');
    console.log(req.params.id);
    var id = req.params.id;
    var queryStrFieldtxt = 'f={"_id":0}&';
    //let queryString = `q={"user_id": ${id} }&`;
    var queryStringID = 'q={"id_user":' + id + '}&';
    var queryCta = 'f={"account":1, "_id":0}&';
    const httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' + queryStringID + queryCta + apikey_mlab,
        function (error, res_mlab, body){
          var respuesta = {};
          respuesta = !error ? body : {"msg":"Usuario no tiene cuentas"};
          res.send(respuesta);
        });
    });

app.post(URL_BASE + "users", //post de user con MLAB
     function(req, res) {
      var  clienteMlab = request_json.createClient(URL_mLab);
      clienteMlab.get('user?'+ apikey_mlab,
      function(error, respuestaMLab , body) {
          newID= body.length+1;
          console.log("newID:" + newID);
          var newUser = {
            "id_user" : newID+1,
            "first_name": req.body.first_name,
            "last_name": req.body.last_name,
            "email": req.body.email,
            "password": req.body.password
          };
          clienteMlab.post(URL_mLab + "user?" + apikey_mlab, newUser ,
           function(error, respuestaMLab, body) {
            res.send(body);
         });
      });
    });


//Delete con id
app.delete(URL_BASE + 'users/:id',
function(request,response){
  console.log("entra al delete");
  console.log("request.params.id: "+ request.params.id);
  var id = request.params.id;
  var queryStringID2 = 'q={"id_user":' + id + '}&';
  console.log(URL_BASE + 'user?' + queryStringID2 + apikey_mlab);
  const httpClient = request_json.createClient(URL_mLab);
  httpClient.get('user?' +  queryStringID2 + apikey_mlab,
     function(error, respuestaMLab, body){
       let respuesta = body[0];
       console.log("body delete:"+ respuesta);
       httpClient.delete(URL_mLab + "user/"+ respuesta._id.$oid +'?'+ apikey_mlab,
         function(error, respuestaMLab,body){
           response.send(body);
       });
     });
});


//Method POST login
app.post(URL_BASE + "login",
  function (req, res){
    console.log("POST login con MLAB");
    var email= req.body.email;
    var pass= req.body.password;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var queryStringpass='q={"password":' + pass + '}&';
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryStringEmail+apikey_mlab,
    function(error, respuestaMLab , body) {
      console.log("entro al body:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session={"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log('{"$set":' + JSON.stringify(session) + '}');
            console.log(URL_mLab +'?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab);
            clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab , JSON.parse(login),
             function(error, respuestaMLab, body) {
              res.send(body[0]);
            });
          }
          else {
            res.send({"msg":"contraseña incorrecta"});
          }
      }else{
        console.log("Email Incorrecto");
        res.send({"msg": "email Incorrecto"});
      }
    });
});


// LOGOUT - MLAB
app.post(URL_BASE + "logout",
  function (req, res){
    console.log("Post Logout con MLAB");
    var email= req.body.email;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var  clienteMlab = request_json.createClient(URL_mLab);
    clienteMlab.get('user?'+ queryStringEmail+apikey_mlab ,
    function(error, respuestaMLab , body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
            console.log("logout Correcto");
            var session={"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
             function(errorP, respuestaMLabP, bodyP) {
              res.send(body[0]);
              //res.send({"msg": "logout Exitoso"});
            });
      }else{
        console.log("Error en logout");
        res.send({"msg": "Error en logout"});
      }
    });
});
